// BusquedaBinaria.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <windows.h>


// Funci�n recursiva de b�squeda binaria. Esta retorna la ubicaci�n
// de x en el array arr[limInferior..limSuperior] si est� presente, // sino devuelve -1
int binarySearch(int arr[], int limInferior, int limSuperior, int x)
{
  if (limSuperior >= limInferior)
  {
    int medio = limInferior + (limSuperior - limInferior)/2;
    // Si el elemento est� presente en el medio
    if (arr[medio] == x) return medio;

    // Si el elemento es menor que el medio, este solo puede estar
    // presente en el sub-array izquierdo
    if (arr[medio] > x) 
      return binarySearch(arr, limInferior, medio-1, x);

    // Sino el elemento puede solo estar presente en el sub-array
    // derecho
    return binarySearch(arr, medio + 1, limSuperior, x);
  }

  // Aqu� se llega cuando el elemento no est� presente en el array
  return -1;
}

/**
 *	Obtener tiempo en milisegundos.
 */
LONG getTimeMsecs() {
  SYSTEMTIME time;
  GetSystemTime(&time);
  LONG time_ms = (time.wMinute * 60000) + (time.wSecond * 1000) + time.wMilliseconds;
  printf("%lu\n", time_ms);
  return time_ms;
}

/**
 *	Main.
 */
int main(void)
{
  
  // Array de numeros aleatorios con 1000 elementos.
  int arr[1000];
  int sum = 0;
  for(int i = 0; i < 1000; ++i)
  { 
	sum += rand() % 3;
	arr[i] = sum;
  }

  int n = sizeof(arr)/ sizeof(arr[0]);
  // x es el nro. a buscar en la coleccion
  int x = 100;
  LONG inicial = getTimeMsecs();
  int result = binarySearch(arr, 0, n-1, x);
  LONG final = getTimeMsecs();
  (result == -1)? printf("Elemento no encontrado en array \n")
    : printf("Elemento presente en indice %d \n", result);
  // Detiene la consola
  printf("%lu\n", (final - inicial));
  _getch();
  return 0;
}